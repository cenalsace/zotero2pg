### Import Zotero data into a PostgreSQL Database

Requirements :
- PostgreSQL
- Python3
- Python3 modules : look at `requirements.txt`

```sh
cp app.cfg.example app.cfg
```

and edit the `app.cfg` with the PostgreSQL connexion informations.
The PostgreSQL password will be asked at the start of the `zotero2pg.py` execution.

