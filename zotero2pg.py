#! /usr/bin/python3

import sqlite3
from configparser import ConfigParser
from getpass import getpass

import pandas as am
from sqlalchemy import create_engine

config = ConfigParser()
config.read("app.cfg")

params = dict(config["PG_CSA"].items())
params["password"] = getpass(prompt="PG Pass: ")

engine = create_engine(
    "postgresql+psycopg2://{user}:{password}@{host}:{port}/{name}".format(**params),
    connect_args={"connect_timeout": 10},
    echo=False,
    pool_recycle=5,
    # future=True,
)

# $HOME/Zotero/zotero.sqlite
sqlite_con = sqlite3.connect("zotero.sqlite")
df_tables = am.read_sql_query(
    "SELECT * FROM sqlite_master WHERE type='table'", sqlite_con
)

data_dfs = {}
for table in df_tables.name.to_list():
    df = am.read_sql_query(f"SELECT * FROM {table}", sqlite_con)
    df.rename(columns={c: str(c).lower() for c in df.columns}, inplace=True)
    data_dfs[table.lower()] = df

for table_name, df_table in data_dfs.items():
    df_table.to_sql(
        table_name, engine, schema="zotero", if_exists="replace", index=False
    )
