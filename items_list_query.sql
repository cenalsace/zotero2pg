SELECT
    bib.itemid, bib.name AS group_name, bib.description AS group_description,
    bib.attached_file,
    fields.title,
    REGEXP_REPLACE(fields.abstractnote, E'[\\n\\r]+', ' ', 'g') AS abstractnote,
    fields.date, fields.language, fields.shorttitle,
    fields.archive, fields.archivelocation,
    fields.librarycatalog, fields.callnumber, fields.url,
    fields.accessdate, fields.rights, fields.extra,
    fields.seriestitle, fields.volume, fields.numberofvolumes,
    fields.place, fields.publisher, fields.runningtime,
    fields.isbn, fields.pages, fields.publicationtitle,
    fields.series, fields.seriesnumber, fields.edition,
    fields.numpages, fields.booktitle, fields.proceedingstitle,
    fields.doi, fields.distributor, fields.videorecordingformat,
    fields.issue, fields.seriestext, fields.journalabbreviation,
    fields.issn, fields.presentationtype, fields.meetingname,
    fields.reportnumber, fields.reporttype, fields.institution,
    fields.nameofact, fields.thesistype, fields.university,
    fields.websitetitle,
    bib.dateadded, bib.datemodified, bib.tags
FROM (
SELECT
    it.itemid, gr.name, gr.description,
    CASE WHEN iat.storagehash IS NOT NULL THEN 'x' END AS attached_file,
    it.dateadded, it.datemodified, tags.tags
FROM
    zotero.items it
    LEFT JOIN zotero.groups gr ON it.libraryid = gr.libraryid
    LEFT JOIN zotero.itemattachments iat ON it.itemid = iat.itemid
    LEFT JOIN (
        SELECT itt.itemid, STRING_AGG(tag.name, ', ') AS tags
        FROM
            zotero.itemtags itt
            INNER JOIN zotero.tags tag ON itt.tagid = tag.tagid
        GROUP BY itt.itemid) tags ON it.itemid = tags.itemid
) bib
LEFT JOIN (
    SELECT
        itd.itemid,
        MAX(CASE WHEN fld.fieldname = 'title' THEN itv.value END) AS title,
        MAX(CASE WHEN fld.fieldname = 'abstractNote' THEN itv.value END) AS abstractnote,
        MAX(CASE WHEN fld.fieldname = 'date' THEN itv.value END) AS date,
        MAX(CASE WHEN fld.fieldname = 'language' THEN itv.value END) AS language,
        MAX(CASE WHEN fld.fieldname = 'shortTitle' THEN itv.value END) AS shorttitle,
        MAX(CASE WHEN fld.fieldname = 'archive' THEN itv.value END) AS archive,
        MAX(CASE WHEN fld.fieldname = 'archiveLocation' THEN itv.value END) AS archivelocation,
        MAX(CASE WHEN fld.fieldname = 'libraryCatalog' THEN itv.value END) AS librarycatalog,
        MAX(CASE WHEN fld.fieldname = 'callNumber' THEN itv.value END) AS callnumber,
        MAX(CASE WHEN fld.fieldname = 'url' THEN itv.value END) AS url,
        MAX(CASE WHEN fld.fieldname = 'accessDate' THEN itv.value END) AS accessdate,
        MAX(CASE WHEN fld.fieldname = 'rights' THEN itv.value END) AS rights,
        MAX(CASE WHEN fld.fieldname = 'extra' THEN itv.value END) AS extra,
        MAX(CASE WHEN fld.fieldname = 'seriesTitle' THEN itv.value END) AS seriestitle,
        MAX(CASE WHEN fld.fieldname = 'volume' THEN itv.value END) AS volume,
        MAX(CASE WHEN fld.fieldname = 'numberOfVolumes' THEN itv.value END) AS numberofvolumes,
        MAX(CASE WHEN fld.fieldname = 'place' THEN itv.value END) AS place,
        MAX(CASE WHEN fld.fieldname = 'publisher' THEN itv.value END) AS publisher,
        MAX(CASE WHEN fld.fieldname = 'runningTime' THEN itv.value END) AS runningtime,
        MAX(CASE WHEN fld.fieldname = 'ISBN' THEN itv.value END) AS isbn,
        MAX(CASE WHEN fld.fieldname = 'pages' THEN itv.value END) AS pages,
        MAX(CASE WHEN fld.fieldname = 'publicationTitle' THEN itv.value END) AS publicationtitle,
        MAX(CASE WHEN fld.fieldname = 'series' THEN itv.value END) AS series,
        MAX(CASE WHEN fld.fieldname = 'seriesNumber' THEN itv.value END) AS seriesnumber,
        MAX(CASE WHEN fld.fieldname = 'edition' THEN itv.value END) AS edition,
        MAX(CASE WHEN fld.fieldname = 'numPages' THEN itv.value END) AS numpages,
        MAX(CASE WHEN fld.fieldname = 'bookTitle' THEN itv.value END) AS booktitle,
        MAX(CASE WHEN fld.fieldname = 'proceedingsTitle' THEN itv.value END) AS proceedingstitle,
        MAX(CASE WHEN fld.fieldname = 'DOI' THEN itv.value END) AS doi,
        MAX(CASE WHEN fld.fieldname = 'distributor' THEN itv.value END) AS distributor,
        MAX(CASE WHEN fld.fieldname = 'videoRecordingFormat' THEN itv.value END) AS videorecordingformat,
        MAX(CASE WHEN fld.fieldname = 'issue' THEN itv.value END) AS issue,
        MAX(CASE WHEN fld.fieldname = 'seriesText' THEN itv.value END) AS seriestext,
        MAX(CASE WHEN fld.fieldname = 'journalAbbreviation' THEN itv.value END) AS journalabbreviation,
        MAX(CASE WHEN fld.fieldname = 'ISSN' THEN itv.value END) AS issn,
        MAX(CASE WHEN fld.fieldname = 'presentationType' THEN itv.value END) AS presentationtype,
        MAX(CASE WHEN fld.fieldname = 'meetingName' THEN itv.value END) AS meetingname,
        MAX(CASE WHEN fld.fieldname = 'reportNumber' THEN itv.value END) AS reportnumber,
        MAX(CASE WHEN fld.fieldname = 'reportType' THEN itv.value END) AS reporttype,
        MAX(CASE WHEN fld.fieldname = 'institution' THEN itv.value END) AS institution,
        MAX(CASE WHEN fld.fieldname = 'nameOfAct' THEN itv.value END) AS nameofact,
        MAX(CASE WHEN fld.fieldname = 'thesisType' THEN itv.value END) AS thesistype,
        MAX(CASE WHEN fld.fieldname = 'university' THEN itv.value END) AS university,
        MAX(CASE WHEN fld.fieldname = 'websiteTitle' THEN itv.value END) AS websitetitle
    FROM
        zotero.itemdata itd
        INNER JOIN zotero.fields fld ON itd.fieldid = fld.fieldid
        INNER JOIN zotero.itemdatavalues itv ON itd.valueid = itv.valueid
    GROUP BY itd.itemid
) fields ON bib.itemid = fields.itemid
ORDER BY bib.itemid
;
